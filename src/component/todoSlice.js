import { createSlice } from '@reduxjs/toolkit'


export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [],
    },
    reducers: {
        loadTodoList: (state, action) => {
            state.todoList = action.payload
        },
    },
})

export const { loadTodoList } = todoSlice.actions

export default todoSlice.reducer