import TodoItem from "./TodoItem"
import { useSelector } from 'react-redux'
import '../css/todo.css'


const TodoGroup = () => {

    const todoList = useSelector(state => state.todo.todoList)

    return (
        <div className='todoList'>
            {
                todoList.map((todoItem) => (
                    <TodoItem key={todoItem.id} todoItem={todoItem}></TodoItem>
                ))
            }
        </div>
    )
}

export default TodoGroup