import { Link } from 'react-router-dom'
import '../css/todo.css'
import { Menu } from 'antd'
import {
    ContainerOutlined,
    DesktopOutlined,
    PieChartOutlined,
} from '@ant-design/icons'

const Navigator = () => {

    return (
        <div className="navigator">
            <Menu mode="inline">
                <Menu.Item key="home" icon={<ContainerOutlined/>}>
                    <Link to="/">Home</Link>
                </Menu.Item>
                <Menu.Item key="help" icon={<DesktopOutlined/>}>
                    <Link to="/help">Help</Link>
                </Menu.Item>
                <Menu.Item key="done" icon={<PieChartOutlined/>}>
                    <Link to="/done">DoneList</Link>
                </Menu.Item>
            </Menu>
        </div>
    )
}

export default Navigator