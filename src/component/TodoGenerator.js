import { useState } from 'react'
import '../css/todo.css'
import useTodo from './../hooks/useTodo';

const TodoGenerator = () => {
    const { add } = useTodo();

    const [ItemStr, setItemStr] = useState('')

    const handleClick = async () => {
        if (ItemStr.trim() !== '') {
            add(ItemStr)
            setItemStr('')
        }
    }

    return (
        <div>
            <input type="text"
                className="todoInput"
                placeholder=" What do you wanna do"
                value={ItemStr}
                onChange={e => setItemStr(e.target.value)}></input>

            <button className="addButton" onClick={handleClick}>add</button>
        </div>
    )
}

export default TodoGenerator