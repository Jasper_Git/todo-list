import TodoGenerator from './TodoGenerator'
import { useEffect } from 'react';
import TodoGroup from './TodoGroup'
import '../css/todo.css'
import useTodo from './../hooks/useTodo';

const TodoList = () => {

    const { getAll } = useTodo();

    const gainTodoList = async () => {
        getAll()
    }

    useEffect(() => {
        gainTodoList()
    })

    return (
        <div className="box">
            <span className="todoTitle">Todo List</span>
            <TodoGroup></TodoGroup>
            <TodoGenerator></TodoGenerator>
        </div>
    )
}

export default TodoList