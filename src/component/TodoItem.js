import '../css/todo.css'
import useTodo from './../hooks/useTodo'
import { useState } from 'react'
import { Button, Modal, Input, message, Popconfirm } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'

const { TextArea } = Input

const TodoItem = ({ todoItem }) => {

  const [text, setText] = useState('')

  const { remove, updateStatus, update } = useTodo()

  const [isModalOpen, setIsModalOpen] = useState(false)
  const showModal = () => {
    setText(todoItem.text)
    setIsModalOpen(true)
  }
  const handleOk = () => {
    setIsModalOpen(false)
    update(todoItem.id, text)
    message.success('Edit Successfully!')
  }
  const handleCancel = () => {
    setIsModalOpen(false)
    message.error('Cancel Delete')
  }

  const doneStyle = {
    textDecoration: "line-through"
  }

  const handleRemoveTodo = async () => {
    remove(todoItem.id)
  }

  const handleChangeDone = () => {
    updateStatus(todoItem.id, !todoItem.done)
  }
  const onChange = (e) => {
    setText(e.target.value)
  }

  const confirm = () => {
    handleRemoveTodo()
    message.success('Delete Successfully!')
  }
  const cancel = () => {
    message.error('Delete Cancel')
  }

  return (
    <div>
      <div key={todoItem.id} className='item' style={todoItem.done ? doneStyle : {}}>
        <span onClick={handleChangeDone}>{todoItem.text}</span>
        <span className="removeButton">
          <Button icon={<EditOutlined />} onClick={showModal}></Button>
          <Popconfirm
            placement="rightBottom"
            title="Delete the task"
            description="Are you sure to delete this task?"
            onConfirm={confirm}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
            <Button icon={<DeleteOutlined />} ></Button>
          </Popconfirm>
        </span>
      </div>

      <Modal title="Edit Todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <TextArea value={text} showCount maxLength={100} onChange={onChange} /><br /><br />
      </Modal>
    </div>
  )
}

export default TodoItem