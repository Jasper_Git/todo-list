import { useParams } from "react-router-dom"
import { Descriptions, Badge } from 'antd'
import '../css/todo.css'
import { useState } from 'react'
import { getItem } from '../api/Todo'

const DoneDetail = () => {
    const { id } = useParams()

    const [text, setText] = useState();

    getItem(id).then(data => {
        const text = data.data.text
        setText(text)
    })
    .catch(error => {

    });

    return (
        <div className="doneDetail">
            <Descriptions title="Done Detail Info" bordered>
                <Descriptions.Item label="id">{id}</Descriptions.Item>
                <Descriptions.Item label="text">{text}</Descriptions.Item>                
                <Descriptions.Item label="status" span={8}>
                    <Badge status="error" text="done" />
                </Descriptions.Item>
            </Descriptions>
        </div>
    )
}

export default DoneDetail