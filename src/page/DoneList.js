
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { List } from 'antd'

const DoneList = () => {
    const doneList = useSelector(state => state.todo.todoList).filter(item => item.done === true)

    const navigate = useNavigate()

    return (
        <div className='doneList'>
            <List
                size="large"
                header={<div>Done List</div>}
                bordered
                dataSource={doneList}
                renderItem={
                    (doneItem) =>
                        <List.Item onClick={() => { navigate(`/done/${doneItem.id}`) }} key={doneItem.id}>
                            {doneItem.text}
                        </List.Item>
                }
            />
        </div>
    )
}

export default DoneList