import { useDispatch } from 'react-redux'
import { getTodoList, addTodoItem, removeTodoItem, updateItemStauts, getItem, updateTodoItem } from '../api/Todo'
const { loadTodoList } = require("../component/todoSlice")


const useTodo = () => {
    const dispatch = useDispatch()

    const getAll = async () => {
        const { data } = await getTodoList()
        dispatch(loadTodoList(data))
    }

    const add = async (text) => {
        await addTodoItem(text)
        getAll()
    }

    const remove = async (id) => {
        await removeTodoItem(id)
        getAll()
    }

    const updateStatus = async (id, done) => {
        await updateItemStauts(id, done)
        getAll()
    }

    const get = async (id) => {
        const { data } = await getItem(id)
        return data
    }

    const update = async (id, text) => {
        await updateTodoItem(id, text)
        getAll()
    }

    return {
        getAll, add, remove, updateStatus, get, update
    }
}

export default useTodo