import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import store from './component/store'
import { Provider } from 'react-redux'
import HelpPage from './page/Help'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import NotFoundPage from './page/NotFound';
import DoneList from './page/DoneList'
import TodoList from './component/TodoList'
import DoneDetail from './page/DoneDetail'

const root = ReactDOM.createRoot(document.getElementById('root'))
const routes = [
  {
    path: '/',
    element: <App />,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: '/help',
        element: <HelpPage />
      },
      {
        path: '/done',
        element: <DoneList />
      },
      {
        path: '/done/:id',
        element: <DoneDetail />
      },
    ]
  },
  {
    path: '*',
    element: <NotFoundPage />
  }
]

const router = createBrowserRouter(routes)

root.render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
)