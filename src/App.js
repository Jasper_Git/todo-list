
import { Outlet } from 'react-router-dom'
import './App.css'
import Navigator from './component/Navigator'

function App() {
  return (
    <div className="App">
      <Navigator></Navigator>
      <Outlet></Outlet>
    </div>
  )
}

export default App
