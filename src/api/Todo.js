import axios from 'axios'

const instanceNew = axios.create(
    {
        baseURL: 'http://localhost:8081/'
    }
)

export const getTodoList = () => {
    return instanceNew.get('/todos')
}

export const addTodoItem = (text) => {
    return instanceNew.post('/todos', {
        text
    })
}

export const removeTodoItem = (id) => {
    return instanceNew.delete(`todos/${id}`)
}

export const updateItemStauts = (id, done) => {
    return instanceNew.put(`todos/${id}`, {
        "done": done
    })
}

export const getItem = (id) => {
    return instanceNew.get(`/todos/${id}`)
}

export const updateTodoItem = (id, text) => {
    return instanceNew.put(`todos/${id}`, {
        "text": text
    })
}