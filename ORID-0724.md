## Objective

- In the morning, I briefly learned cloud native, used concept maps to summarize and practice what I learned last week, and later introduced HTML and CSS selectors and box models.
- In the afternoon, I learned some principles of React componentization, JSX, and the state improvement of components.

## Reflective

- Today is a relatively easy day, even if there are some problems with Java Script and ES6 syntax.

## Interpretive

- Benefits of React componentization:
  - First of all, the most straightforward is code reuse
  - Improve maintainability, each component focuses on its own responsibilities, so that the code can be better debugged, modified, and maintained
  - Improved testability and easier writing of tests for individual components
  - Enhance scalability by adding messengers or adjusting existing features to operate only on the corresponding components
- When multiple components need to access and modify the same state, this state can be promoted to their common parent and the state can be passed to the child components through props. In this way, all subcomponents can read and update this state, maintaining state consistency.

## Decisional

- Need to spend more energy on learning Javascript, React, ES6